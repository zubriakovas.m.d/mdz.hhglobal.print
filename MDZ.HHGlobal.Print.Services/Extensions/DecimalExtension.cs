﻿using System;

namespace MDZ.HHGlobal.Print.Services.Extensions;

public static class DecimalExtension
{
    public static double ToCent(this double number) =>
        Math.Round(number, 2);
    public static double ToEvenCent(this double number) =>
        (0.02 / 1.00) * double.Round(number * (1.00 / 0.02));
}