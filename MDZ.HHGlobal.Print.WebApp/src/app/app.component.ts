import { Component } from '@angular/core';
import {JobPriceService} from "./services/jobPriceService";
import {PrintJobPriceRequest} from "./models/printJobPriceRequest";
import {PrintJobItemRequest} from "./models/printJobItemRequest";
import {PrintJobPriceResponse} from "./models/printJobPriceResponse";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'MDZ.HHGlobal.Print.WebApp';
  public request: PrintJobPriceRequest;
  public response?: PrintJobPriceResponse
  constructor(
    private service: JobPriceService
  ) {
    this.request = new PrintJobPriceRequest();
    this.request.items.push(new PrintJobItemRequest());
  }

  addJobItem(){
    this.request.items.push(new PrintJobItemRequest());
  }

  removeJobItem(index: number){
    this.request.items = this.request.items.splice(index);
  }

  calculatePrice(){
    this.service.getPrice(this.request)
      .subscribe(x=> this.response = x);
  }
}
