# MDZ.HHGlobal.Print


## Structure
- Documents - contains task documentation.

- MDZ.HHGlobal.Print.API - contains:
  - Tax/Margin configuration in `appsettings.json` file
  - Typical API with a controller to call `/Job/calculatePrice` endpoint, accepting JSON of
    ```
      { 
        "extraMargin": true, 
        "items": 
        [ 
          {
            "name": "stamps",
            "amount": 100.00,
            "taxExempt": true
          }
        ]
      }
- MDZ.HHGlobal.Print.Entities - contains:
  - Request/response classes
  - API configuration classes

- MDZ.HHGlobal.Print.Services - contains:
  - Price rounding logic extension
  - Tax and Margin calculation service interface/class
- MDZ.HHGlobal.Print.Tests - contains unit tests for provided scenarios 

- MDZ.HHGlobal.Print.WebApp - contains Angular application for testing purposes

## Requirements

- .NET SDK 7.0.7
- Angular CLI: 16.2.6
- Node: 18.18.2
- Package Manager: npm 9.8.1

## Getting started

1. Clone the repository
2. Build and Run the API project
   - It should be running on https://localhost:44385/ 
     - that is what the web application is targeting in `src/environments/environment.ts` file through `apiUrl` parameter
6. Locate the WebApp project
    `cd MDZ.HHGlobal.Print.WebApp`
7. Run `npm i` then run `ng serve` 
8. Open your browser and go to http://localhost:4200/