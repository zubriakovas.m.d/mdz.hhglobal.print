﻿namespace MDZ.HHGlobal.Print.Entities;

public class PriceConfiguration
{
    public double SalesTax { get; set; }
    public double Margin { get; set; }
    public double ExtraMargin { get; set; }
}