﻿using System.Collections.Generic;

namespace MDZ.HHGlobal.Print.Entities;

public class PrintJobPriceResponse
{
    public List<PrintJobItem> Items { get; set; }
    public double TotalAmount { get; set; }
}