using System;
using System.Collections.Generic;
using System.Linq;
using MDZ.HHGlobal.Print.Entities;
using MDZ.HHGlobal.Print.Services.Interfaces;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MDZ.HHGlobal.Print.API.Controllers;

[ApiController]
[Route("[controller]")]
public class JobController : ControllerBase
{
    private readonly ILogger<JobController> _logger;
    private readonly IPrintJobPriceService _jobPriceService;
    
    public JobController(ILogger<JobController> logger, IPrintJobPriceService jobPriceService)
    {
        _logger = logger;
        _jobPriceService = jobPriceService;
    }

    [EnableCors]
    [HttpPost("calculatePrice")]
    public PrintJobPriceResponse CalculatePrice([FromBody]PrintJobPriceRequest request)
    {
        return _jobPriceService.CalculateJobPrice(request);
    }
}