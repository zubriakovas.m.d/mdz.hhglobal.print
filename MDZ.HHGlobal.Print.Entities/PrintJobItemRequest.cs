﻿namespace MDZ.HHGlobal.Print.Entities;

public class PrintJobItemRequest : PrintJobItem
{
    public bool TaxExempt { get; set; }
}