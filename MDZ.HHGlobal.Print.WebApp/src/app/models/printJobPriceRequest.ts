﻿import {PrintJobItemRequest} from "./printJobItemRequest";

export class PrintJobPriceRequest {
  public items: PrintJobItemRequest[];
  public extraMargin: boolean;

  public constructor() {
    this.items = []
    this.extraMargin = false;
  }
}
