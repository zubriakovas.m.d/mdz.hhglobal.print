﻿
import {PrintJobPriceRequest} from "../models/printJobPriceRequest";
import {PrintJobPriceResponse} from "../models/printJobPriceResponse";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
@Injectable({providedIn: "root"})
export class JobPriceService {
  constructor(private http:HttpClient) {
  }

  public getPrice(request: PrintJobPriceRequest){
    return this.http.post<PrintJobPriceResponse>(environment.apiUrl + '/Job/calculatePrice', request);
  }
}
