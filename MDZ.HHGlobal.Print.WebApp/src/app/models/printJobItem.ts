﻿export class PrintJobItem {
  public name: string;
  public amount: number;

  public constructor() {
    this.name = '';
    this.amount = 0;
  }
}
