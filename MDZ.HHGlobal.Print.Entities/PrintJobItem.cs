﻿namespace MDZ.HHGlobal.Print.Entities;

public class PrintJobItem
{
    public string Name { get; set; }
    public double Amount { get; set; }
}