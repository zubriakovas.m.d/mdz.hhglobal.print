﻿import {PrintJobItem} from "./printJobItem";

export class PrintJobItemRequest extends PrintJobItem {
  public taxExempt: boolean;

  public constructor() {
    super();
    this.taxExempt = false;
  }
}
