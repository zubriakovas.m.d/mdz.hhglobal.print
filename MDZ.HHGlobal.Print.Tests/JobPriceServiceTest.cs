using System.Collections.Generic;
using MDZ.HHGlobal.Print.Entities;
using MDZ.HHGlobal.Print.Services;
using MDZ.HHGlobal.Print.Services.Interfaces;
using Microsoft.Extensions.Options;
using NUnit.Framework;

namespace MDZ.HHGlobal.Print.Tests;

public class JobPriceServiceTest
{
    private IPrintJobPriceService PrintJobPriceService { get; set; }
    [SetUp]
    public void Setup()
    {
        PrintJobPriceService = new PrintJobPriceService(Options.Create(new PriceConfiguration()
        {
            Margin = 0.11,
            ExtraMargin = 0.05,
            SalesTax = 0.07
        }));
    }

    [Test]
    public void Job1()
    {
        var result = PrintJobPriceService.CalculateJobPrice(new PrintJobPriceRequest()
        {
            ExtraMargin = true,
            Items = new List<PrintJobItemRequest>()
            {
                new ()
                {
                    Name = "envelopes",
                    TaxExempt = false,
                    Amount = 520.00
                },
                new ()
                {
                    Name = "letterhead",
                    TaxExempt = true,
                    Amount = 1983.37
                }
            }
        });
        
        Assert.AreEqual(556.40, result.Items[0].Amount, 0.001);
        Assert.AreEqual(1983.37, result.Items[1].Amount, 0.001);
        Assert.AreEqual(2940.30, result.TotalAmount, 0.001);
    }
    
    [Test]
    public void Job2()
    {
        var result = PrintJobPriceService.CalculateJobPrice(new PrintJobPriceRequest()
        {
            ExtraMargin = false,
            Items = new List<PrintJobItemRequest>()
            {
                new ()
                {
                    Name = "t-shirts",
                    TaxExempt = false,
                    Amount = 294.04
                }
            }
        });
        
        Assert.AreEqual(314.62, result.Items[0].Amount, 0.001);
        Assert.AreEqual(346.96, result.TotalAmount, 0.001);
    }

    [Test]
    public void Job3()
    {
        var result = PrintJobPriceService.CalculateJobPrice(new PrintJobPriceRequest()
        {
            ExtraMargin = true,
            Items = new List<PrintJobItemRequest>()
            {
                new ()
                {
                    Name = "frisbees",
                    TaxExempt = true,
                    Amount = 19385.38
                },
                new ()
                {
                    Name = "yo-yos",
                    TaxExempt = true,
                    Amount = 1829.00
                },
            }
        });
        
        
        Assert.AreEqual(19385.38, result.Items[0].Amount, 0.001);
        Assert.AreEqual(1829.00, result.Items[1].Amount, 0.001);
        Assert.AreEqual(24608.68, result.TotalAmount, 0.001);
    }
}