﻿using System;
using System.Linq;
using MDZ.HHGlobal.Print.Entities;
using MDZ.HHGlobal.Print.Services.Extensions;
using MDZ.HHGlobal.Print.Services.Interfaces;
using Microsoft.Extensions.Options;

namespace MDZ.HHGlobal.Print.Services;

public class PrintJobPriceService: IPrintJobPriceService
{
    private readonly PriceConfiguration _priceConfiguration;

    public PrintJobPriceService(IOptions<PriceConfiguration> priceConfiguration)
    {
        _priceConfiguration = priceConfiguration.Value;
    }
    
    public PrintJobPriceResponse CalculateJobPrice(PrintJobPriceRequest priceRequest)
    {
        var responseItems = priceRequest.Items.Select(x => new PrintJobItem()
        {
            Name = x.Name,
            Amount = (x.Amount + CalculateItemTax(x)).ToCent()
        }).ToList();
        
        return new PrintJobPriceResponse()
        {
            Items = responseItems,
            TotalAmount =  (responseItems.Sum(x => x.Amount) + CalculateJobMargin(priceRequest)).ToEvenCent() 
        };
    }

    private double CalculateItemTax(PrintJobItemRequest item) =>
        item.TaxExempt ? 0.00 : item.Amount * _priceConfiguration.SalesTax;

    private double CalculateJobMargin(PrintJobPriceRequest priceRequest) =>
        priceRequest.Items.Sum(x => x.Amount) * (priceRequest.ExtraMargin ? 
            _priceConfiguration.Margin + _priceConfiguration.ExtraMargin : 
            _priceConfiguration.Margin);
}