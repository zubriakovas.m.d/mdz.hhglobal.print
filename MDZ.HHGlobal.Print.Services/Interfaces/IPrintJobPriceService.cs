﻿using MDZ.HHGlobal.Print.Entities;

namespace MDZ.HHGlobal.Print.Services.Interfaces;

public interface IPrintJobPriceService
{
    public PrintJobPriceResponse CalculateJobPrice(PrintJobPriceRequest priceRequest);
}