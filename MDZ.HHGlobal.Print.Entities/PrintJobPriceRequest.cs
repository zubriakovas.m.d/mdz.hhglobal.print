﻿using System.Collections.Generic;

namespace MDZ.HHGlobal.Print.Entities;

public class PrintJobPriceRequest
{
    public bool ExtraMargin { get; set; }
    public List<PrintJobItemRequest> Items { get; set; }
}