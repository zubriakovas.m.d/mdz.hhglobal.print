﻿import {PrintJobItem} from "./printJobItem";

export class PrintJobPriceResponse {
  public items: PrintJobItem[];
  public totalAmount: number;

  public constructor() {
    this.items = [];
    this.totalAmount = 0;
  }
}
